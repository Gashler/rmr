<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Order;
use App\Repositories\UserRepository;
use App\User;

class OrderController extends Controller
{
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function store()
    {
        // get data
        $data = request()->all();

        // charge card
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $charge = \Stripe\Charge::create(array(
          "amount" => $data['amount'] * 100,
          "currency" => "usd",
          "description" => env('APP_NAME'),
          "source" => $data['token'],
        ));

        // create / update user
        if ($user = User::where('email', $data['email'])->first()) {
            $user->update([
                'name' => $data['name'],
                'message' => $data['message'],
                'total_amount' => $user->total_amount + $data['amount']
            ]);
        } else {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'message' => $data['message'],
                'total_amount' => $data['amount']
            ]);
        }

        // determine rank
        $users = User::orderby('total_amount', 'DESC')->get();
        $users = $this->userRepo->determineRanks($users);
        foreach ($users as $u) {
            if ($user->id == $u->id) {
                $user = $u;
                break;
            }
        }

        // create order
        Order::create([
            'user_id' => $user->id,
            'amount' => $data['amount']
        ]);

        return [
            'user' => $user
        ];
    }
}
