<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\User;

class UserController extends Controller
{
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    // index
    public function index()
    {
        $params = request()->all();
        if (isset($params['limit'])) {
            $users = User::offset($params['offset'])->orderBy('total_amount', 'DESC')->limit($params['limit'])->get();
        } else {
            $users = User::offset($params['offset'])->orderBy('total_amount', 'DESC')->get();
        }
        return $this->userRepo->determineRanks($users, $params['offset']);
    }
}
