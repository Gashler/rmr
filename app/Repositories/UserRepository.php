<?php

namespace App\Repositories;

class UserRepository
{
    public function determineRanks($users, $offset = 0)
    {
        foreach ($users as $index => $user) {
            $rank = $index + 1 + $offset;
            if ($rank >= 4 && $rank <= 20) {
                $suffix = 'th';
            } else {
                $lastDigit = substr($rank, -1);
                if ($lastDigit == 1) $suffix = 'st';
                elseif ($lastDigit == 2) $suffix = 'nd';
                elseif ($lastDigit == 3) $suffix = 'rd';
                else $suffix = 'th';
            }
            $user->rank = $rank;
            $user->formatted_rank = $rank . $suffix;
            $users[$index] = $user;
        }
        return $users;
    }
}
