<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $messages = [
            "You rock, Robby!",
            "You & me, Robby, BFF",
            "Jaime, will you marry me???",
            "His wires flow with love for you. His circuits always will be true.",
            "Robby and I go way back.",
            "This is the best site ever. Best money I ever spent.",
            "I've always wanted to be Robby's best friend.",
            "Luv U Robz!",
            "For real, I can't believe I didn't know about this sooner. So good.",
            "My life now has meaning.",
            "Robby is so magical. He loves every one of us.",
            "I wish I could have given more.",
            "Yo, dogz, check out my tunez. Looke me up on CCMixter.",
            "Robby and I go way back. I'll spot him some cash any time.",
            "Robby, I just want to let you know that you're my hero."
        ];
        User::truncate();
        $faker = Faker::create();
        foreach (range(1,15) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'message' => $messages[($index - 1)],
                'email' => $faker->email,
                'city' => $faker->city,
                'province' => $faker->state,
                'country' => $faker->country,
                'total_amount' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 1, $max = 25)
            ]);
        }
    }
}
