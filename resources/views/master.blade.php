<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Robby the Magic Robot</title>
        <link rel="stylesheet" href="/css/theme.css">
        <link href="https://fonts.googleapis.com/css?family=Oswald:300" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <script src="https://checkout.stripe.com/checkout.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta property="og:url" content="{{ env('APP_URL') }}">
        <meta property="og:title" content="Robby the Magic Robot">
        <meta property="og:image" content="{{ env('APP_URL') }}/img/og-img.jpg">
        <meta property="og:description" content="Robby the Magic Robot is our friend. You can be his friend to for just 5 tokens.">
    </head>
    <body>
        @section('content')
        @show
        <section id="footer">
            Copyright &copy; {{ date('Y') }} {{ env('APP_NAME') }}
        </section>
        <!-- Google Anlaytics tracking -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-4655820-19', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>
