@extends('master')
@section('content')
<div ng-app="app" ng-controller="HomeController">
    <audio autoplay controls loop controlsList="nodownload" controlsList="nomute" controlsList="novolume">
        <source src="/robby-the-magic-robot.mp3" type="audio/mpeg">
    </audio>
    <h1>How Much do YOU Love Robby?</h1>
    <div id="robby">
        <div id="screen">
            <h2>My Best Friends</h2>
            <img ng-show="loading" src="/img/loading.gif">
            <div ng-show="!loading" ng-cloak>
                <a ng-show="display == 'all'" id="see-all" ng-click="getUsers('top')">See My Top 10 Friends</a>
                <table id="users">
                    <tr ng-repeat="u in users" id="@{{ u.id }}" ng-class="{ 'highlight': u.rank <= 3 || user.id == u.id, 'strong': u.rank == 1 }">
                        <td>
                            @{{ u.rank }}.<span ng-if="$index < 9">&nbsp;&nbsp;</span>
                            <a ng-click="showUser(u)">@{{ u.name }}</a>
                        </td>
                        <td>@{{ u.total_amount | currency}}</td>
                    </tr>
                </table>
                <a ng-show="display == 'top'" id="see-all" ng-click="getUsers('all')">See All of My Friends</a>
            </div>
        </div>
    </div>
    <br>
    <button class="btn btn-primary" data-toggle="modal" data-target="#userForm">
        Become Robby's Friend
    </button>
    <div class="modal modal-primary fade align-left" id="userForm">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Become Robby's Friend</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group" style="max-width: 300px;">
                        <label>Your Name</label>
                        <input type="text" ng-model="name" placeholder="(As you'd like it displayed)" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Your Message</label>
                        <textarea ng-model="message" placeholder="(Optional)" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>How much do you love Robby?</label>
                        <div class="input-group" style="max-width: 125px;">
                            <span class="input-group-addon">$</span>
                            <input type="text" ng-model="amount" placeholder="5.00" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" id="togglePaymentForm">
                            Pay Robby
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-primary fade align-left" id="success">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Success</h3>
                </div>
                <div class="modal-body">
                    <p>
                        Congratulations, you are now Robby's @{{ user.formatted_rank }} best friend!
                    </p>
                    <div class="form-group">
                        <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-primary fade align-left" id="userShow">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>@{{ user.name }} - Robby's @{{ user.formatted_rank }} Best Friend</h3>
                </div>
                <div class="modal-body">
                    <p>@{{ user.name }} loves Robby to the amount of @{{ user.total_amount | currency }}.</p>
                    <label>@{{ user.name }}'s Message:</label>
                    <div class="well">
                        @{{ user.message }}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var stripe_key = '{{ env('STRIPE_KEY') }}';
</script>
<script src="/js/controllers/HomeController.js"></script>
@stop
