@extends('master')
@section('content')
    <br>
    <h1>Your Download is Ready!</h1>
    <p>Download your stock music library through the link below.</p>
    <p>Bookmark this page in case you need to redownload.</p>
    <a
        href="https://www.dropbox.com/s/exlbak8xpvdet30/Instrumentals.zip?dl=1"
        class="btn btn-primary"
        style="max-width: 300px; margin: 0 auto;"
        download
    >Download <i class="fa fa-download"></i></a>
    <p style="margin-top: 1em; font-family: arial; font-size: 10pt;">(2.79 Gigabytes - this might take a while)</p>
    <!-- Google Code for Sale-99 Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 853693060;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "JAWCCLyaknEQhKWJlwM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/853693060/?label=JAWCCLyaknEQhKWJlwM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
@stop
