// home controller
var app = angular.module('app', []);
angular.module('app').controller('HomeController', function($scope, $http, $interval) {

    $scope.scaleRobby = function()
    {
        var width = $(window).width();
        if (width < 402) {
            var percent = width / 402;
            $('#robby').css({
                'transform': 'scale(' + percent + ')'
            });
        }
    }
    $(window).resize(function() {
        $scope.scaleRobby();
    });
    $scope.scaleRobby();

    $scope.getUsers = function(display)
    {
        $scope.loading = true;
        $scope.display = display;
        if (display == 'all') {
            var offset = 10;
            var limit = 10000;
        }
        if (display == 'top') {
            var offset = 0;
            var limit = 10;
        }
        var url = '/users?offset=' + offset + '&limit=' + limit;
        $http.get(url).then(function(response) {
            $scope.users = response.data;
            $scope.loading = false;
        });
    }
    $scope.getUsers('top');

    $scope.showUser = function(user)
    {
        $scope.user = user;
        $('#userShow').modal('show');
    }

    // scroll to
    $scope.scrollTo = function(element)
    {
        $('html, body').animate({
            scrollTop: $(element).offset().top
        }, 2000);
    }

    /******************
    * Stripe Functions
    *******************/

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var handler = StripeCheckout.configure({
      key: stripe_key,
      locale: 'auto',
      token: function(token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
      }
    });

    document.getElementById('togglePaymentForm').addEventListener('click', function(e) {
      // Open Checkout with further options:
      $('#userForm').modal('hide');

      handler.open({
        amount: $scope.amount * 100,
        allowRememberMe: false,
        description: "Become Robby's Friend",
        // panelLabel: '',
        // zipCode: true,
        // billingAddress: true,
        token: function(response) {
            if (response.id) {
                $scope.loading = true;
                $http.post('/process-order', {
                    name: $scope.name,
                    message: $scope.message,
                    amount: $scope.amount,
                    email: response.email,
                    token: response.id,
                    card: response.card.id
                }).then(function(response) {
                    console.log('response = ', response);
                    $scope.user = response.data.user;
                    $scope.users = response.data.users;
                    $('#success').modal('show');
                    $scope.loading = false;
                    if ($scope.user.rank > 10) {
                        $scope.display = 'all';
                    } else {
                        $scope.display = 'top';
                    }
                    $scope.getUsers($scope.display);
                    // $scope.scrollTo('#' + $scope.user.id);
                });
            }
        }
      });
      e.preventDefault();
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
        handler.close();
    });
});
